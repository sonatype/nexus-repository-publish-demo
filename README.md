#### bitbucket-nexus-platform-pipe-demo

This is a demo repository which makes use of the `sonatype/bitbucket-platform-pipe` pipe.

See https://bitbucket.org/sonatype/bitbucket-nexus-platform-pipe/
